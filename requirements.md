
# Starting Point

We are building a SaaS system for "Subscriptions". Vendors offer goods or services through our system. Customers purchase an automatically renewing subscription.

We support web and mobile customer-facing applications. We email vendors with their daily activity.

---

# Basic Requirements

- Vendors define goods & services to offer
  - Vendor can define period of delivery (week, month, quarter, year)
  - Customer can select timing when ordering
- We bill the customer, then pay the vendor
  - Billing/renewal period may be different than the delivery period
  - Plans allow monthly, quarterly, annual
  - Vendors may offer discount tiers for longer terms
- Credit cards accepted
- We send credit card expiration warnings
- We send shipment orders to the vendor
- We send shipment notices to customer
- We offer customer service by phone

---

# Per-Vendor Customization

- Calendar / holidays
- Countries serviced, currencies accepted
- Number and lead time of expiration notices
